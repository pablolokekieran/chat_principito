import tkinter
from tkinter import scrolledtext 
import asyncio
import paho.mqtt.client as mqtt
import datetime
CHANNEL = "faef3e443baec53aff8e798ff459c764"

def on_closing():
    print("Close")
    loop.stop()
    
    raiz.destroy()


raiz = tkinter.Tk()
raiz.protocol("WM_DELETE_WINDOW", on_closing)
Bframe = tkinter.Frame(raiz , bg = 'red')

view = tkinter.scrolledtext.ScrolledText(Bframe , state = tkinter.DISABLED)
tmsg = tkinter.StringVar()
tname = tkinter.StringVar()


def send():
    buffer = list(tmsg.get())
    _str = ''
    
    for _byte in buffer:
        
        _str += chr(ord(_byte) ^ 0b1101100)
        

    
    client.publish(CHANNEL,str(datetime.datetime.now().strftime("%H:%M")) + " " + tname.get()+ ": " + _str)
    tmsg.set("")

def window():
    top_Frame = tkinter.Frame(raiz)
    top_Frame.pack(side = tkinter.TOP, fill = "x")

    top0_Frame = tkinter.Frame(top_Frame , bg = 'blue')
    top0_Frame.pack(side = 'top', fill = 'x' )

    top1_Frame = tkinter.Frame(top_Frame , bg = 'red')
    top1_Frame.pack(side = 'top', fill = 'x' )
    
    name_label = tkinter.Label(top0_Frame, text = "Nombre")
    name_label.pack(side = tkinter.LEFT)

    name_entry = tkinter.Entry(top0_Frame, textvariable = tname)
    name_entry.pack(side = tkinter.LEFT)

    
    entry = tkinter.Entry(top1_Frame, textvariable = tmsg)
    entry.pack(side = 'left',fill = 'x', expand = True)
    
    send_button = tkinter.Button(top1_Frame, text = 'Send', command = send)
    send_button.pack(side = 'left')
    
    #view = tkinter.Label(Bframe)
    Bframe.pack(side = 'top', fill = 'both' ,expand = True)

    
    #view.config(textvariable = tTexto)
    view.pack( fill ='both' , expand = True)

async def gui():
    while True:
        await asyncio.sleep(0.02)    
        raiz.update()

async def count():
    c = 0
    while True:
        client.loop(0.01)
        await asyncio.sleep(0.5)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("faef3e443baec53aff8e798ff459c764")

   

def on_message(client, userdata, msg):
    _msg = msg.payload
    print(str(msg.payload))
    view.config(state = tkinter.NORMAL)
    view.insert(tkinter.END, _msg.decode("UTF-8") + '\n')
    view.config(state = tkinter.DISABLED)
    view.see(tkinter.END)

if __name__ == '__main__':
    window()
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("test.mosquitto.org", 1883, 60)

    loop = asyncio.get_event_loop()
    ##loop.run_until_complete(asyncio.gather(gui(),count()))
    try:
        asyncio.ensure_future(count())
        asyncio.ensure_future(gui())
        loop.run_forever()
    finally:    
        loop.close()
    
